<?php

use Illuminate\Database\Seeder;
// use DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      DB::table('categories')->insert(
        [

         ['name' => 'T-shirt', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Long Sleeve', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Coat Jacket', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Jacket', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Pants', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Skirt', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Long skirt', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Blouse', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Sleeveless', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Crop top', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Shorts', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Leggings', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Polo', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Polo shirt', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],
         ['name' => 'Others', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s') ],

       ]
     );
    }
  }
