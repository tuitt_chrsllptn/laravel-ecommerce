<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert(
        	[
        		['status_name' => 'Pending', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],
        		['status_name' => 'Shipped', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],
        		['status_name' => 'Delivered', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')],
        		['status_name' => 'Cancelled', 'created_at' => Now()->format('Y-m-d H:i:s'), 'updated_at' => Now()->format('Y-m-d H:i:s')]
        	]
        );
    }
}
