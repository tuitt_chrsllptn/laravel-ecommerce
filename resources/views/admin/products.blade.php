@extends('template')
@section('title', 'Admin')
@section('content')
<div class="container">
  {{-- Add product form --}}
  <div class="row">
    <div class="col-md-8 offset-md-2">
      {{-- handles validation --}}
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      {{-- handles success and delete message --}}

      @if(Session::has("success_msg"))
      <div class="alert alert-success" role="alert">
        {{ Session::get("success_msg") }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @elseif(Session::has("delete_msg"))
      <div class="alert alert-danger" role="alert">
        {{ Session::get("delete_msg") }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif

      <form action="/addproduct" method="POST" enctype="multipart/form-data">
        @csrf
        <h3 class="text-center">Add product</h3>

        <div class="form-group">
          <label>Product name:</label>
          <input type="text" name="productname" class="form-control">
        </div>

        <div class="form-group">
          <label>Product description</label>
          <textarea name="productdescription" id="" cols="30" rows="10" class="form-control"></textarea>
        </div>

        <div class="form-group">
          <label>Product price:</label>
          <input type="number" name="productprice" class="form-control">
        </div>

         <div class="form-group">
          <label>Product image:</label>
          <input type="file" name="image" class="form-control">
        </div>

        <div class="form-group">
          <label>Product category:</label>
          <select class="form-control" name="productcategory" id="">
            @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
          </select>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-primary form-control">Add product</button>
        </div>
      </form>
    </div>
  </div>

  {{-- Retrieve products --}}
  <div class="row">
    <div class="col-md-10 offset-md-1">
      <table class="table">
        <thead>
          <tr>
            <th>Product name</th>
            <th>Description</th>
            <th>Catetgory</th>
            <th>Price</th>
            <th>Update/Delete</th>
          </tr>
        </thead>
        <tbody>
          @foreach($products as $product)
          <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->category_id }}</td>
            <td>{{ $product->price }}</td>
            <td>
              {{-- button to trigger update modal --}}
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#updateModal{{ $product->id }}">Edit</button>

              {{-- button to trigger delete modal --}}
              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $product->id }}">Delete</button>
            </td>
          </tr>

          {{-- Modal forms --}}
          {{-- Update modal form --}}
          <div class="modal fade" id="updateModal{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Update a product</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form action="/updateproduct/{{ $product->id }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                      <label>Product name:</label>
                      <input type="text" name="updateproductname" value="{{ $product->name }}" class="form-control">
                    </div>

                    <div class="form-group">
                      <label>Product description</label>
                      <textarea name="updateproductdescription" id="" cols="30" rows="10" class="form-control">{{ $product->description }}</textarea>
                    </div>

                    <div class="form-group">
                      <label>Product price:</label>
                      <input type="number" name="updateproductprice" class="form-control" value="{{ $product->price }}">
                    </div>

                    <div class="form-group">
                      <label>Product category:</label>
                      <select class="form-control" name="updateproductcategory" id="">
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary form-control">Update product</button>
                    </div>
                  </form>
                </div>
                <div class="modal-footer justify-content-center">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>

          {{-- Delete modal form --}}

          <div class="modal fade" id="deleteModal{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Delete {{ $product->name }}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  Do you really want to delete this product?
                  <br>
                  <small>You cannot undo this changes.</small>
                </div>
                <div class="modal-footer">
                  <form action="/deleteproduct/{{ $product->id }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection