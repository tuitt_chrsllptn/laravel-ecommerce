@extends('template')
@section('title', 'Catalog')
@section('content')
<div class="container-fluid">
	{{-- Page Content --}}
	<div class="container">
		<div class="row">
			<div class="col">
				<table class="table">
					<thead>
						<tr>
							<td>Transaction Code</td>
							<td>Date</td>
							<td>Products</td>
							<td>Quantity</td>
							<td>Price</td>
							<td>Status</td>
						</tr>
					</thead>
					<tbody>
						@foreach($orders as $order)
						@if(Auth::user()->id == $order->user_id)
						@foreach($order->products as $orderproducts)
						<tr>
							<td>
								{{ $order->transaction_code }}
							</td>
							<td>
								{{ $orderproducts->pivot->created_at->diffForHumans() }}
							</td>
							<td>
								{{ $orderproducts->name }}
							</td>
							<td>
								{{ $orderproducts->pivot->quantity }}
							</td>
							<td>
								{{ $orderproducts->pivot->price }}
							</td>
							<td>
								{{ $order->status->status_name }}
							</td>
						</tr>
						@endforeach
						@endif
						@endforeach

					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection