@extends('template')
@section('title', 'Catalog')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			@include('products.sidebar')
		</div>
		<div class="col-md-8">
			@include('products.card_products')
		</div>
	</div>
</div>

@endsection