@extends('template')
@section('title', 'Catalog')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col">
			
			@if (Session::has('success'))
			<div class="jumbotron">
				<h4 class="display-4">
					{{ Session::get('success')}}
				</h4>
				<p>Your reference no: <strong>{{ $transaction_code }}</strong></p>
				<a href="/catalog" class="btn btn-primary btn-lg">Go back to shopping</a>	
			</div>
			@elseif(Session::has('error'))
			<h1 class="display-4">
				{{ Session::get('error')}}
			</h1>
			<a href="/catalog/showcart" class="btn btn-primary btn-lg">Go back to cart</a>	
			@endif
		</div>
	</div>
</div>
@endsection