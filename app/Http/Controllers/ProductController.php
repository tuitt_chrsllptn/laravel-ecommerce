<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Order;
use Auth;
use Session;

//paypal API
use PayPal;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use URL;
use PayPal\Api\Payment;
use Redirect;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $showproducts = Product::all();
        $showcategories = Category::all();
        return view('products.catalog', compact('showproducts', 'showcategories'));
    }

    
    public function store(Request $request, $prodId)
    {
        //check if there are existing items inside the Session cart
        if (Session::has('cart')) {
            // if there's any, we'll get it and give it to $cart variable
            $cart = Session::get('cart');
        } else {
            // we'll be creating an empty array called $cart
            $cart = [];
        }

        if (isset($cart[$prodId])) {
            //add new quantity to existing item in the session cart
            $cart[$prodId] += $request->quantity;
        } else {
            $cart[$prodId] = $request->quantity;
        }

        Session::put('cart', $cart);
        $product = Product::find($prodId);
        Session::flash("success_cart", "$request->quantity of $product->name successfully added to cart!");
        return redirect('/catalog');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $products_in_cart = [];
        $total = 0;
        if (Session::has('cart')) {

            $cart = Session::get('cart');
            foreach ($cart as $id => $quantity) {
                $products = Product::find($id);
                //At this point, each element of the $product has id, name, description, price, img_path, category_id AND quantity, subtotal.
                //quantity and subtotal DO NOT EXIST in the database, but it exists for $product.
                $products->quantity = $quantity;
                $products->subtotal = $products->price * $quantity;
                $total += $products->subtotal;
                //We push to the empty array the $item.
                $products_in_cart[] = $products;
            }
        }

        return view('products.cart_content', compact("products_in_cart", "total"));
    }


    public function update(Request $request, $prodId)
    {
       $cart = Session::get("cart");
       $cart[$prodId] = $request->newquantity;
       Session::put("cart", $cart);
       return redirect("/catalog/showcart");

   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($prodId)
    {
        Session::forget("cart.$prodId");
        return redirect("/catalog/showcart");
    }

    public function emptycart(){
        Session::forget("cart");
        return redirect("/catalog/showcart");
    }

    public function filter($catId){
        $showcategories = Category::all();
        $showproducts = Product::where('category_id', $catId)->get();

        return view('products.catalog', compact('showproducts', 'showcategories'));
        
    }

    public function cashOnDelivery(){
        $order = new Order;
        $order->user_id = Auth::user()->id;
        $order->status_id = 1; //all orders should start pending
        // $order->payment_method = "COD";
        $order->transaction_code = "NEWHORIZON".time();
        $order->save();

        //attach on pivot table product_order
        foreach (Session::get('cart') as $product_id => $quantity) {
         $products_session = Product::find($product_id);
         $products_session->subtotal = $products_session->price * $quantity;
         $order->products()->attach($product_id, ['quantity'=> $quantity, 'price' => $products_session->subtotal]);
     }

     Session::forget("cart");
     return redirect("/catalog");

     }

    //  public function payWithpaypal(Request $request){
    //     /** PayPal api context **/
    //     $paypal_conf = \Config::get('paypal');
    //     $this->_api_context = new ApiContext(new OAuthTokenCredential(
    //         $paypal_conf['client_id'],
    //         $paypal_conf['secret'])
    // );
    //     $this->_api_context->setConfig($paypal_conf['settings']);

    //     //the terminology like Payer, Item is all from the PayPal SDK
    //     // Payer - resource representing a Payer that funds a payment For PayPal account payments, set payment method to ‘paypal’.
    //     $payer = new Payer();
    //     $payer->setPaymentMethod('paypal');
    //             //Item information (Optional) Lets you specify item wise information
    //     $item_1 = new Item();
    //     $item_1->setName('Item 1') /** item name **/
    //     ->setCurrency('USD')
    //     ->setQuantity(1)
    //     ->setPrice($request->get('amount')); /** unit price **/
    //     $item_list = new ItemList();
    //     $item_list->setItems(array($item_1));

    //     //Amount Lets you specify a payment amount. You can also specify additional details such as shipping, tax.
    //     $amount = new Amount();
    //     $amount->setCurrency('USD')
    //     ->setTotal($request->get('amount'));

    //     // Transaction - A transaction defines the contract of a payment — what is the payment for and who is fulfilling it.
    //     $transaction = new Transaction();
    //     $transaction->setAmount($amount)
    //     ->setItemList($item_list)
    //     ->setDescription('Your transaction description');

    //     // Redirect URLs - Set the URLs that the buyer must be redirected to after payment approval/ cancellation.
    //     $redirect_urls = new RedirectUrls();
    //     $redirect_urls->setReturnUrl(URL::route('catalog')) /** Specify return URL **/
    //     ->setCancelUrl(URL::route('showcart'));

    //     // Payment - A Payment Resource; create one using the above types and intent set to ‘sale’
    //     $payment = new Payment();
    //     $payment->setIntent('Sale')
    //     ->setPayer($payer)
    //     ->setRedirectUrls($redirect_urls)
    //     ->setTransactions(array($transaction));
    //     /** dd($payment->create($this->_api_context));exit; **/

    //     //Create an order to the orders table
    //     $order = new Order;
    //     $order->user_id = Auth::user()->id;
    //             $order->status_id = 1; //all orders should start pending
    //             $order->payment_method = "COD";
    //             $order->transaction_code = "NEWHORIZON".time();
    //             $order->save();

    //             //attach on pivot table product_order
    //             foreach (Session::get('cart') as $product_id => $quantity) {
    //              $products_session = Product::find($product_id);
    //              $products_session->subtotal = $products_session->price * $quantity;
    //              $order->products()->attach($product_id, ['quantity'=> $quantity, 'price' => $products_session->subtotal]);
    //          }


    //          try {
    //             // Create Payment

    //             // Create a payment by calling the ‘create’ method passing it a valid apiContext. (See bootstrap.php for more on ApiContext) The return object contains the state and the URL to which the buyer must be redirected to for payment approval
    //             $payment->create($this->_api_context);

    //             // Session::forget("cart");
    //             // return redirect("/catalog");
    //            //end of order
    //         } catch (\PayPal\Exception\PPConnectionException $ex) {
    //             if (\Config::get('app.debug')) {
    //                 \Session::put('error', 'Connection timeout');
    //                 return Redirect::route('paywithpaypal');
    //             } else {
    //                 \Session::put('error', 'Some error occur, sorry for inconvenient');
    //                 return Redirect::route('paywithpaypal');
    //             }
    //         }
    //         foreach ($payment->getLinks() as $link) {
    //             if ($link->getRel() == 'approval_url') {
    //                 $redirect_url = $link->getHref();
    //                 break;
    //             }
    //         }
    //         /** add payment ID to session **/
    //         Session::put('paypal_payment_id', $payment->getId());
    //         if (isset($redirect_url)) {
    //             /** redirect to paypal **/
    //             return Redirect::away($redirect_url);
    //         }
    //         \Session::put('error', 'Unknown error occurred');
    //         return Redirect::route('paywithpaypal');
    //     }

    public function checkoutpaypal(Request $request){

        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
    );
        $this->_api_context->setConfig($paypal_conf['settings']);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName('Items') /** item name **/
        ->setCurrency('USD')
        ->setQuantity(1)
        ->setPrice($request->get('amount')); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        // dd($request->get('amount'));
        $amount->setCurrency('USD')
        ->setTotal($request->get('amount'));
        $transaction = new Transaction();
        $transaction->setAmount($amount.'00')
        ->setItemList($item_list)
        ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('catalog')) /** Specify return URL **/
        ->setCancelUrl(URL::route('catalog'));
        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirect_urls)
        ->setTransactions(array($transaction));
        dd($payment->create($this->_api_context)); 

       
        try {
            $payment->create($this->_api_context);
            
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return redirect('/menu/mycart');
            } else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return redirect('/menu/mycart');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
     
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }


        \Session::put('error', 'Unknown error occurred');
        return Redirect::route('showcart');
    }

    public function showHistory(){
        $orders = Order::all();

        return view('products.transactionhistory', compact('orders'));
    }
}
