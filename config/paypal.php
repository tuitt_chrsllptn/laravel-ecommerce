<?php 
return [ 
 'client_id' => env('PAYPAL_CLIENT_ID','AXdgXELl0Q72eZi-o8LzEr3DE_BK_OpI_cUZwxOcHhDLQY9G_Fe1hMywuiQsg8pQVK4FsjVqna3u5skz'),
 'secret' => env('PAYPAL_SECRET','EO4IKOMkXlCNpJ71KBDxkGw7B2imQ-jkE5la8xOTWN3PdD0ir47PUayauJ68GOcd8Ziy_Q9ckvNvw3DD'),
'settings' => array(
  'mode' => env('PAYPAL_MODE','sandbox'), //Option 'sandbox' or 'live', sandbox for testing
  'http.ConnectionTimeOut' => 1000, //Max request time in seconds
  'log.LogEnabled' => true, //Whether want to log to a file
  'log.FileName' => storage_path() . '/logs/paypal.log', //Specify the file that want to write on
  'log.LogLevel' => 'FINE' //Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
  /**
  * Logging is most verbose in the 'FINE' level and decreases as you
  * proceed towards ERROR
  */
 ),
];